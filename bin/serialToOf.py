#!/env/python
# coding: utf-8

import liblo
import serial
import time
import random

t = liblo.Address(6669)

s = serial.Serial('/dev/ttyACM0', baudrate=9600, timeout=1)
print 'connected'
time.sleep(2)

lfo = False

mul_random = 0;
min_random = 5;

lst_msg = [
    {'id':'/cycloid/arm_y',
     'l':-200,
     'h':200,
     'v':0.0,
     'min':0.0,
     'max':0.0},
    {'id':'/cycloid/arm_x',
     'l':-100,
     'h':800,
     'v':0.0,
     'min':0.0,
     'max':0.0},
    {'id':'/cycloid/v1',
    'l':-10.0,
     'h':10.0,
     'v':0.0,
     'min':0.0,
    'max':0.0},
    {'id':'/cycloid/v',
     'l':-10.0,
     'h':10.0,
     'v':0.0,
     'min':0.0,
     'max':0.0},
    {'id':'/cycloid/v2',
     'l':-10,
     'h':10,
     'v':0.0,
     'min':0.0,
    'max':0.0},
    {'id':'/cycloid/r1',
     'l':0.0,
     'h':500.0,
     'v':0.0,
     'min':0.0,
     'max':0.0},
    {'id':'/cycloid/r2',
     'l':0.0,
     'h':500.0,
     'v':0.0,
     'min':0.0,
     'max':0.0},
    {'id':'/cycloid/pos1y',
     'l':-3,
     'h':3,
     'val':500,
     'v':random.random() * mul_random + min_random,
     'min':-500.0,
     'max':500.0},
    {'id':'/cycloid/pos1x',
     'l':3,
     'h':-3,
     'val':0,
     'v':random.random() * mul_random + min_random,
     'min':-800.0,
     'max':800.0},
    {'id':'/cycloid/pos2y',
     'l':-3,
     'h':3,
     'val':500,
     'v':random.random() * mul_random + min_random,
     'min':-500.0,
     'max':500.0},           
    {'id':'/cycloid/pos2x',
     'l':3,
     'h':-3,
     'val':1000,
     'v':random.random() * mul_random + min_random,
     'min':-800.0,
     'max':800.0}
]

def anim():
    for i in range(7, len(lst_msg)):
        m = lst_msg[i]
        m['val'] += m['v']
        if m['val'] > m['max']:
            m['val'] = m['max']
            m['v'] = - random.random() * mul_random + min_random
        if m['val'] < m['min']:
            m['val'] = m['min']
            m['v'] = random.random() * mul_random + min_random

        liblo.send(t, m['id'], m['val'])
        s.write(b'%s %s \t\n'%(i, int(m['val'])))
        time.sleep(0.05)
def m(val, low, high):
    return low + val * (high - low) / 1023.0

def redir(message):
    lfo_loc = False
    try:
        if message[0] == "l":
            lfo_loc = True
            print "////////////////////////"
            print message,
        elif message[0] == "n":
            liblo.send(t, '/cycloid/prec_v', 2)
            liblo.send(t, '/cycloid/prec_v1', 2)
        elif message[0] == "m":
            liblo.send(t, '/cycloid/prec_v', 0)
            liblo.send(t, '/cycloid/prec_v1', 0)
        else:
            msg = message.split()
            for i, it in enumerate(lst_msg):
                if int(msg[0]) == i: 
                    lfo = False;
                    if int(msg[0]) < 7:
                        liblo.send(t, it['id'], m(float(msg[1]), it['l'], it['h']))
                    else:
                        it['val'] = float(msg[1])
                        liblo.send(t, it['id'], msg[1])
            print message,
    except:
        pass
    return lfo_loc

while 1:
    if s.inWaiting() > 0:
        lfo = redir(s.readline())
    else:
        if lfo:
            anim()
            print 'lfo', lfo
    time.sleep(0.01)
