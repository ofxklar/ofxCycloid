/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

 This example code is in the public domain.
 */

#include "RunningMedian.h"

#define HWSERIAL Serial1

int n_pot = 9;
int n_pot_an = 5;

struct Pot{
    byte pin;
    RunningMedian med;
    int value;
    int cur;
    int val_min;
    int val_max;
};

float step = 3.0;

Pot pots[9] = {{A0, RunningMedian(8), 0, 0, 0, 0},
               {A1, RunningMedian(8), 0, 0, 0, 0},
               {A2, RunningMedian(8), 0, 0, 0, 0},
               {A3, RunningMedian(8), 0, 0, 0, 0},
               {A4, RunningMedian(8), 0, 0, 0, 0},
               {A5, RunningMedian(8), 0, -500, -800, 800},
               {A6, RunningMedian(8), 0, 0, -500, 500},
               {A7, RunningMedian(8), 0, 500, -800, 800},
               {A8, RunningMedian(8), 0, 0, -500, 500}};

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  delay(1000);
  Serial.begin(9600);
  
  delay(1000);
  HWSERIAL.begin(9600);
  delay(1000);
  while(!Serial.dtr());
  initPot();
}

// the loop routine runs over and over again forever:
void loop() {
  readPot();
}

void initPot(){
  for (int i = 0; i < n_pot ; i++){
    pinMode(pots[i].pin, INPUT);
  }
}

void readPot(){
  for (int i = 0; i < n_pot_an ; i++){
    int v = analogRead(pots[i].pin);
    pots[i].med.add(v);
    int med_v = pots[i].med.getMedian();
    delay(1);
    if (abs(med_v - pots[i].value)>1){
      pots[i].value = med_v;
      Serial.print(i);
      Serial.print(" ");
      Serial.println(v);
      HWSERIAL.print(i);
      HWSERIAL.print(" ");
      HWSERIAL.println(v);
      }

    }
  for (int i = n_pot_an; i < n_pot ; i++){
    int v = analogRead(pots[i].pin);
    delay(1);
    if ((v < 492) or (v > 523)){
      pots[i].value = v;
      pots[i].cur += map(v, 0, 1024, -step, step);
      pots[i].cur = min(pots[i].cur, pots[i].val_max);
      pots[i].cur = max(pots[i].cur, pots[i].val_min);
      Serial.print(i);
      Serial.print(" ");
      Serial.println(pots[i].cur);
      HWSERIAL.print(i);
      HWSERIAL.print(" ");
      HWSERIAL.println(pots[i].cur  );
    }
  }

}

