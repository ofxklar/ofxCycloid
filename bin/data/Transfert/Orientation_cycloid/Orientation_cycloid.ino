// Full orientation sensing using NXP's advanced sensor fusion algorithm.
//
// You *must* perform a magnetic calibration before this code will work.
//
// To view this data, use the Arduino Serial Monitor to watch the
// scrolling angles, or run the OrientationVisualiser example in Processing.

#include <CapacitiveSensor.h>

#include <NXPMotionSense.h>
#include <MadgwickAHRS.h>
#include <Wire.h>
#include <EEPROM.h>

//CapacitiveSensor   touch_1 = CapacitiveSensor(15,16);     // 1M resistor between pins 15 & 16, pin 16 is sensor pin
CapacitiveSensor   touch_2 = CapacitiveSensor(17,20);     // 1M resistor between pins 17 & 20, pin 20 is sensor pin
//CapacitiveSensor   touch_3 = CapacitiveSensor(22,23);     // 1M resistor between pins 22 & 23, pin 23 is sensor pin

long last_touch_1;
long last_touch_2;
long last_touch_3;

long last_t;

long debounce = 50;

int state_touch_1 = 0;

int cpt = 0;


NXPMotionSense imu;
//NXPSensorFusion filter;
Madgwick filter;

int thresh = 2000;

void setup() {
  Serial.begin(9600);
  while (!Serial) ; // wait for serial port open
  imu.begin();
  filter.begin(100);

  last_t = millis();
  
  last_touch_1 = 0;
  last_touch_2 = 0;
  last_touch_3 = 0;
}

void loop() {
  float ax, ay, az;
  float gx, gy, gz;
  float mx, my, mz;
  float roll, pitch, heading;

//  if (millis() - last_t > debounce){

//  }
  if (imu.available()) {
      update_touch();
    // Read the motion sensors
    imu.readMotionSensor(ax, ay, az, gx, gy, gz, mx, my, mz);

    // Update the SensorFusion filter
    //filter.update(gx, gy, gz, ax, ay, az, mx, my, mz);


    // Update the Madgwick filter
    filter.updateIMU(gx, gy, gz, ax, ay, az);

    // print the heading, pitch and roll
    heading = map(filter.getYaw(),     0, 360, 0, 100);
    pitch   = map(filter.getPitch(), -90,  90, 0, 100);
    roll    = map(filter.getRoll(), -180, 180, 0, 100);

    Serial.print(state_touch_1);
    Serial.print("\t");
    Serial.print(heading);
    Serial.print("\t");
    Serial.print(pitch);
    Serial.print("\t");
    Serial.println(roll);
 // }else{
 //   Serial.println("no");
  }
}

void update_touch(){
  
  if (cpt==0){
    //long cur_touch_1 =  touch_1.capacitiveSensor(10);
    long cur_touch_1 = touchRead(16);
        if (last_touch_1 < thresh && cur_touch_1 > thresh){
      state_touch_1 = 1;
//      Serial.println("t1");
//      last_t = millis();
    }else if (cur_touch_1 < thresh){
        state_touch_1 = 0;
    }

    
    last_touch_1 = cur_touch_1;
  }else if (cpt==1){
    long cur_touch_2 =  touch_2.capacitiveSensor(10);
  //  long cur_touch_2 = touchRead(17);
        if (last_touch_2 < 1000 && cur_touch_2 > 1000){
      Serial.println("t2");
      last_t = millis();
    }
    
    last_touch_2 = cur_touch_2;
  }else if (cpt==2){
    //long cur_touch_3 =  touch_3.capacitiveSensor(10);
       long cur_touch_3 = touchRead(23);
        if (last_touch_3 < thresh && cur_touch_3 > thresh){
      Serial.println("t3");
      last_t = millis();
    }
    
    last_touch_3 = cur_touch_3;
  }
  cpt = (cpt+1)%3;


}
