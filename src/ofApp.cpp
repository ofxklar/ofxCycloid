#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

  ofEnableSmoothing();
  ofSetLineWidth(3);
  ofSetVerticalSync(true);
  
  draw_cycloid = true;
  draw_meca = false;

  width_max = 1920;
  height_max = 1080;

  width = ofGetScreenWidth();
  height = ofGetScreenHeight();

  //width = 1368;
  //height = 766;

  // width_meca = 800.0;
  // height_meca = 480.0;
  width_meca = width;
  height_meca = height;


  if (draw_meca){
    ofHideCursor();
    if ((width / width_meca) > (height / height_meca)){
      meca_ratio = width_meca / width;
      pos_meca.set(0, (height_meca - height*meca_ratio)/2);
    }else{
      meca_ratio = float(height) / height_max;
      pos_meca.set((width - width_meca)/2, 0);
    }    
  }

  // init fbo
  fbo.allocate(width, height, GL_RGBA);
  fbo_tex.allocate( width, height, GL_RGBA);

  shader.load("decrease");

  ofSetCircleResolution(100);
  ofSetLineWidth(2);
  ofEnableSmoothing();

  // GUI
  parameters.setName("cycloid");

	preset_gui.addListener(this, &ofApp::presetChanged);
  parameters.add(preset_gui.set("preset", 0, 0, 20));

  parameters.add(draw_nodes.set("draw nodes", false));
  parameters.add(mode_slow.set("mode slow", false));
  parameters.add(draw_monitor.set("draw monitor", false));
  parameters.add(draw_gui.set("draw_gui", false));
  parameters.add(draw_cycloid.set("draw_cycloid", true));
  parameters.add(draw_meca.set("draw_meca", false));

  parameters.add(n_points.set("n_points", 1024, 1, 20000));
  parameters.add(n_points_slow.set("n_points_slow", 1, 1, 10));
  prec_v = 0;
  parameters.add(prec_v.set("prec_v", 0, 0, 3));
  parameters.add(prec_v1.set("prec_v1", 0, 0, 3));
  parameters.add(prec_v2.set("prec_v2", 0, 0, 3));

  parameters.add(v.set("v", -1.05, -10, 10));
  parameters.add(v1.set("v1", 2.1, -10, 10));
  parameters.add(v2.set("v2", 10, -10, 10));

  parameters.add(pos1_x.set("pos1x" , -500, -1000, 1000)); 
  parameters.add(pos1_y.set("pos1y" , 0, -1000, 1000)); 
  parameters.add(r1.set("r1", 100, 0, 1000));

  parameters.add(pos2_x.set("pos2x" , 500, -1000, 1000)); 
  parameters.add(pos2_y.set("pos2y" ,  0, -1000, 1000));
  parameters.add(r2.set("r2", 100, 0, 1000));	  

  parameters.add(arm_x.set("arm x" , 300, -500, 500)); 
  parameters.add(arm_y.set("arm y" ,  100, -500, 500));
  
  parameters.add(v10.set("v10", 1, -10, 10));
  parameters.add(v20.set("v20", 1, -10, 10));
  parameters.add(r10.set("r10", 0, 0, 1000));
  parameters.add(r20.set("r20", 0, 0, 1000));	  
  
  parameters.add(decrease.set("decrease", 4, 0, 10.0));
  parameters.add(fps.set("fps", 0, 0, 60));
	  
  gui.setup(parameters);

  gui_presets.setup("cycloid_presets"); // most of the time you don't need a name
  gui_presets.add(preset.setup("preset", 0, 0, 20));
  sync.setup((ofParameterGroup&)gui.getParameter(),6669,"localhost",6668);

  // Cycloid
  o.setPosition(width/2, height/2, 0);
  p1.setParent(o);
  p2.setParent(o);
  c1.setParent(p1);
  c2.setParent(p2);
  c10.setParent(c1);
  c20.setParent(c2);
  
  o_m.setPosition(width/2, height/2, 0);
  o_m1.setPosition(width/2, height/2, 0);
  o_m2.setPosition(width/2, height/2, 0);
  o_m3.setParent(o_m2);
  o_m4.setParent(o_m2);
  o_m3.setPosition(450, 0, 0);
  o_m4.setPosition(-450, 0, 0);
  p1_m.setParent(o_m);
  p2_m.setParent(o_m);
  c1_m.setParent(p1_m);
  c2_m.setParent(p2_m);
  c10_m.setParent(c1_m);
  c20_m.setParent(c2_m);

  
  p1_m2.setParent(o_m1);
  p2_m2.setParent(o_m1);
  c1_m2.setParent(p1_m2);
  c2_m2.setParent(p2_m2);

  o_s.setPosition(width/2, height/2, 0);
  p1_s.setParent(o_s);
  p2_s.setParent(o_s);
  c1_s.setParent(p1_s);
  c2_s.setParent(p2_s);
  c10_s.setParent(c1_s);
  c20_s.setParent(c2_s);

  
  updatePosition();

  ofPoint pos1 = c10.getGlobalPosition();
  ofPoint pos2 = c20.getGlobalPosition();

  last_pos = pos1 + arm_x * (pos2 - pos1).normalize() +  arm_y * (pos2 - pos1).getPerpendicular(ofVec3f(0,0,1));


  // Meca
  o_color.set(0, 0, 255);
  p1_color.set(145, 0, 174);
  p2_color.set(0, 200, 0);
  arm_color1.set(128);
  arm_color2.set(255, 0, 0);
  b_color.set(0, 128);

  len_m = 300;
  
  for (int i = 0; i<len_m; i++){
    points_m.push_back(ofPoint(0, 0));
    rotation_m.push_back(0.0);
  }

  meca_w = 3;
  
  ll = -(1920-1080)/2;
  rr = 1080 - ll;

  setupAudio();
}
//--------------------------------------------------------------
void ofApp::presetChanged(int & new_preset){
  preset = new_preset;
  gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
}


//--------------------------------------------------------------
void ofApp::setupAudio(){
  gui.add(draw_audio.set("draw audio", false));
  gui.add(mode_audio.set("mode audio", true));

  soundStream.printDeviceList();


  ofSoundStreamSettings settings;

#ifdef TARGET_LINUX
  // Latest linux versions default to the HDMI output
  // this usually fixes that. Also check the list of available
  // devices if sound doesn't work
  auto devices = soundStream.getMatchingDevices("default");
  if(!devices.empty()){
    settings.setOutDevice(devices[0]);
  }
#endif

  settings.setOutListener(this);
  settings.bufferSize    = 512;
  settings.sampleRate    = 44100;
  settings.numOutputChannels = 2;
  settings.numInputChannels = 0;
  settings.numBuffers = 4;

  volume        = 0.5f;

  sampleRate = 44100;
  bufferSize = 512;

  soundStream.setup(settings);
  //soundStream.setup(this, 2, 0, sampleRate, bufferSize, 4);

  lAudio.assign(bufferSize, 0.0);
  rAudio.assign(bufferSize, 0.0);
}

void ofApp::updatePosition(){
  p1.setPosition(pos1_x, pos1_y, 0);
  p2.setPosition(pos2_x, pos2_y, 0);
  c1.setPosition(r1, 0, 0);
  c2.setPosition(r2, 0, 0);
  c10.setPosition(r10, 0, 0);
  c20.setPosition(r20, 0, 0);

  p1_m.setPosition(pos1_x, pos1_y, 0);
  p2_m.setPosition(pos2_x, pos2_y, 0);
  c1_m.setPosition(r1, 0, 0);
  c2_m.setPosition(r2, 0, 0);
  c10_m.setPosition(r10, 0, 0);
  c20_m.setPosition(r20, 0, 0);

  
  p1_m2.setPosition(pos1_x, pos1_y, 0);
  p2_m2.setPosition(pos2_x, pos2_y, 0);
  c1_m2.setPosition(r1, 0, 0);
  c2_m2.setPosition(r2, 0, 0);

  p1_s.setPosition(pos1_x, pos1_y, 0);
  p2_s.setPosition(pos2_x, pos2_y, 0);
  c1_s.setPosition(r1, 0, 0);
  c2_s.setPosition(r2, 0, 0);

  c10_s.setPosition(r10, 0, 0);
  c20_s.setPosition(r20, 0, 0);

  
  if (prec_v > 0){
    v = roundf(float(v)*pow(10, int(prec_v)-1)) / pow(10, int(prec_v)-1);
  }
  if (prec_v1 > 0){
    v1 = roundf(float(v1)*pow(10, int(prec_v1)-1)) / pow(10, int(prec_v1)-1);
  }
  if (prec_v2 > 0){
    v2 = roundf(float(v2)*pow(10, int(prec_v2)-1)) / pow(10, int(prec_v2)-1);
  }
}

//--------------------------------------------------------------
void ofApp::update(){
  sync.update();
  fps = ofGetFrameRate();

  updatePosition();

  correction = ofMap(ofGetLastFrameTime(), 0, 0.02, 0, 1);

  int n_loop = n_points;
  if (mode_slow){ n_loop = n_points_slow;}

  fbo_tex = fbo.getTextureReference();

  int ll = -(1920-1080)/2;
  int rr = 1080 - ll;

  o_m2.rotate(-v*correction, ofVec3f(0,0,1)); 
  o_m1.rotate(v*correction, ofVec3f(0,0,1)); 
  p1_m.rotate(v1*correction, ofVec3f(0,0,1)); 
  p2_m.rotate(v2*correction, ofVec3f(0,0,1)); 

  c1_m.rotate(v10*correction, ofVec3f(0,0,1)); 
  c2_m.rotate(v20*correction, ofVec3f(0,0,1)); 
  
  p1_m2.rotate(v1*correction, ofVec3f(0,0,1)); 
  p2_m2.rotate(v2*correction, ofVec3f(0,0,1)); 

  ofPoint pos1_m = c1_m2.getGlobalPosition();
  ofPoint pos2_m = c2_m2.getGlobalPosition();
  
  int_pos_m = pos1_m + arm_x * (pos2_m - pos1_m).normalize();
  
  pos_m = int_pos_m + arm_y * (pos2_m - pos1_m).getPerpendicular(ofVec3f(0,0,1));

  rotation_m.push_back(v*correction);
  points_m.push_back(pos_m);

  rot_cum += rotation_m[0];

  points_m.erase(points_m.begin());
  rotation_m.erase(rotation_m.begin());

  

  if (draw_cycloid){
    fbo.begin();		
    
    for (int i = 0; i < n_loop; ++i){
        o.rotate(v*correction, ofVec3f(0,0,1)); 
        p1.rotate(v1*correction, ofVec3f(0,0,1)); 
        p2.rotate(v2*correction, ofVec3f(0,0,1)); 

	c1.rotate(v10*correction, ofVec3f(0,0,1));
	c2.rotate(v20*correction, ofVec3f(0,0,1));
	
        ofPoint pos1 = c10.getGlobalPosition();
        ofPoint pos2 = c20.getGlobalPosition();
    
        int_pos = pos1 + arm_x * (pos2 - pos1).normalize();
    
        ofPoint new_pos = int_pos +  arm_y * (pos2 - pos1).getPerpendicular(ofVec3f(0,0,1));
    
    
        ofPushMatrix();
        ofDrawLine(last_pos, new_pos);
        ofPopMatrix();
    
        last_pos = new_pos;  
      }
    
    shader.begin();
    shader.setUniformTexture("texture1", fbo_tex, 1);
    shader.setUniform1f("path_decrease", decrease);
    
    fbo_tex.draw(0,0);
    shader.end();
    fbo.end();
  }
}

//--------------------------------------------------------------
void ofApp::draw(){
  ofBackground(0);

  ofNoFill();
  ofSetColor(255);

  if (draw_nodes){
    p1.draw();
    c1.draw();
    p2.draw();
    c2.draw();
  }
  
  if (draw_cycloid){
    fbo.draw(0, 0);
  } 

  if (draw_gui){
    gui.draw();
    gui_presets.draw();
  }

  if (draw_audio){
    drawAudio();
  }

  if (draw_monitor){
    drawMonitor();
  }

  if (draw_meca){
    drawMeca();
  }
}


//--------------------------------------------------------------
void ofApp::drawMonitor(){

  ofPushStyle();
  ofSetColor(150);
  ofDrawCircle(p1.getGlobalPosition(), 10);
  ofDrawCircle(p2.getGlobalPosition(), 10);

  ofSetColor(0,0,255);
  ofDrawCircle(p1.getGlobalPosition(), r1);
  ofDrawCircle(p2.getGlobalPosition(), r2);

  ofDrawCircle(c1.getGlobalPosition(), r10);
  ofDrawCircle(c2.getGlobalPosition(), r20);

  
  ofSetColor(150);
  ofDrawCircle(c1.getGlobalPosition(), 10);
  ofDrawCircle(c2.getGlobalPosition(), 10);

  ofDrawCircle(c10.getGlobalPosition(), 10);
  ofDrawCircle(c20.getGlobalPosition(), 10);
  
  ofDrawLine(c10.getGlobalPosition(), c20.getGlobalPosition());
  
  ofSetColor(0, 255, 0);
  ofDrawLine(c10.getGlobalPosition(), int_pos);
  ofDrawLine(int_pos, last_pos);

  ofPopStyle();
}

void ofApp::drawMeca(){
  int margin = 30;
  ofPushStyle();
  ofPushMatrix();
  ofTranslate(pos_meca);
  ofScale(meca_ratio, meca_ratio);
  ofSetColor(254);
  
  ofSetLineWidth(meca_w);
  
  ofNoFill();
  ofSetColor(o_color);

  ofNoFill();
  ofDrawLine(o_m4.getGlobalPosition(), o_m3.getGlobalPosition());
  ofDrawCircle(o.getGlobalPosition(), 14);
  //ofDrawCircle(o.getGlobalPosition(), 400);

  ofFill();
  ofSetColor(0);
  ofDrawCircle(o.getGlobalPosition(), 12);

  // Roue 2

  ofSetColor(p2_color);
  ofNoFill();
  ofDrawCircle(p2_m.getGlobalPosition(), r2 + margin); // disque

  ofSetColor(b_color);
  ofFill();
  ofDrawCircle(p2_m.getGlobalPosition(), r2 + margin - 2); // disque int

  ofSetColor(p2_color);
  ofNoFill();
  ofDrawLine(p2_m.getGlobalPosition(), c2_m.getGlobalPosition()); // ligne
  ofDrawCircle(p2_m.getGlobalPosition(), 14); // pivots
  ofDrawCircle(c2_m.getGlobalPosition(), 14);

  ofSetColor(0);
  ofFill();
  ofDrawCircle(p2_m.getGlobalPosition(), 12); // pivots int
  ofDrawCircle(c2_m.getGlobalPosition(), 12);

  ofDrawCircle(c20_m.getGlobalPosition(), 12);

  
  // Roue 1

  ofSetColor(p1_color);
  ofNoFill();
  ofDrawCircle(p1_m.getGlobalPosition(), r1 + margin); // disque

  ofSetColor(b_color);
  ofFill();
  ofDrawCircle(p1_m.getGlobalPosition(), r1 + margin - 2); // disque int

  ofSetColor(p1_color);
  ofNoFill();
  ofDrawLine(p1_m.getGlobalPosition(), c1_m.getGlobalPosition()); // ligne
  ofDrawCircle(p1_m.getGlobalPosition(), 14); // pivots
  ofDrawCircle(c1_m.getGlobalPosition(), 14);
  ofDrawCircle(c10_m.getGlobalPosition(), 14);

  ofSetColor(0);
  ofFill();
  ofDrawCircle(p1_m.getGlobalPosition(), 12); // pivots int
  ofDrawCircle(c1_m.getGlobalPosition(), 12);


  ofVec2f vec_0(1, 0);
  ofVec2f vec_1 = c10_m.getGlobalPosition();
  ofVec2f vec_2 = c20_m.getGlobalPosition();

  int dist_max = glm::distance(p1.getGlobalPosition(), p2.getGlobalPosition()) + r1 + r2;
  
  int dist_min = dist_max - 2 * (r1+r2);
  if (dist_min < 0){
    dist_min = 0;
  }
  

  // bras
  int arm_0 = 0;
  if (arm_x < 0){
    arm_0 = arm_x;
  }else if(arm_x > dist_max){
    dist_max = arm_x;
  }

  ofPushMatrix();
  ofTranslate(c1_m.getGlobalPosition());  

  ofRotate(vec_0.angle(vec_2-vec_1));

  ofSetColor(arm_color1);
  ofFill();
  ofDrawCircle(arm_0, 0, margin+6);
  ofDrawCircle(dist_max, 0, margin+6); 
  ofDrawRectangle(arm_0, -margin-6, dist_max - arm_0, 2*margin+12);
  
  ofDrawCircle(arm_x, -arm_y, margin+6);
  ofDrawRectangle(arm_x - margin - 6, 0, 2*margin+12, -arm_y);

  ofSetColor(0);
  ofFill();
  ofDrawCircle(arm_0, 0, margin);
  ofDrawCircle(dist_max, 0, margin);
  ofDrawRectangle(arm_0, -margin, dist_max - arm_0, 2*margin);
  ofDrawCircle(arm_x, -arm_y, margin);
  ofDrawRectangle(arm_x - margin, 0, 2*margin, -arm_y);


  ofSetColor(arm_color1);
  ofDrawLine(dist_min, 0, dist_max, 0);

  ofSetColor(arm_color2);
  ofDrawLine(0, 0, arm_x, 0);
  ofDrawLine(arm_x, 0, arm_x, -arm_y);

  ofSetColor(255);
  ofDrawCircle(arm_x, -arm_y, 15);
  
  ofSetColor(0);
  ofDrawCircle(arm_x, -arm_y, 10);
  


  //ofDrawRectangle(arm_x - margin, margin, 2 * margin, -arm_y - 2*margin);

  ofPopMatrix();



  // pivot 2
  ofSetColor(p2_color);
  ofNoFill();
  ofDrawCircle(c2_m.getGlobalPosition(), 14);

  ofSetColor(0);
  ofFill();
  ofDrawCircle(c2_m.getGlobalPosition(), 12);

  // pivot 1
  ofSetColor(p1_color);
  ofNoFill();
  ofDrawCircle(c1_m.getGlobalPosition(), 14);

  ofSetColor(0);
  ofFill();
  ofDrawCircle(c1_m.getGlobalPosition(), 12);

  ofNoFill();
  ofSetColor(255);
  ofSetLineWidth(2);
  ofBeginShape();

  ofTranslate(width/2, height/2);
  ofRotate(-rot_cum);
  ofTranslate(-width/2, -height/2);
  for (int i = 0; i < points_m.size(); i++){
    ofSetColor(ofMap(i, 0, len_m, 0, 255));
    ofTranslate(width/2, height/2);
    ofRotate(-rotation_m[i]);
    ofTranslate(-width/2, -height/2);
    ofVertex(points_m[i]);
  }

  ofEndShape();

  ofPopMatrix();
  ofPopStyle();
  // memo angle 
  //ofVec2f v0(1, 0);
  //ofVec2f v1(pos1_x, pos1_y);
  //ofRotate(v1.angle(v0));
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
  switch(key){
  case 'f':
    ofToggleFullscreen();
    break;
  case 'n':
    draw_nodes = !draw_nodes;
    break;
  case 's':
    mode_slow = !mode_slow;
    break;
  case 'g':
    draw_gui = !draw_gui;
    break;
  case 'm':
    draw_monitor = !draw_monitor;
    break;
  case 'r':
    gui.saveToFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 'l':
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_RIGHT:
    preset = min(preset + 1, 20);
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_LEFT:
    preset = max(preset - 1, 0);
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case OF_KEY_UP:
    preset = min(preset + 1, 20);
    break;
  case OF_KEY_DOWN:
    preset = max(preset - 1, 0);
    break;
  case '&':
    preset = 0;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 233:
    preset = 1;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '\"':
    preset = 2;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '\'':
    preset = 3;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '(':
    preset = 4;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '-':
    preset = 5;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 232:
    preset = 6;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case '_':
    preset = 7;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 231:
    preset = 8;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  case 224:
    preset = 9;
    gui.loadFromFile("presets/preset_" + ofToString((int)preset) + ".xml");
    break;
  }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

//--------------------------------------------------------------
void ofApp::drawAudio(){
  if (draw_audio){

    int posx = ofGetWidth() * 0.1;
    int posy = ofGetHeight() * 0.9;
    int sizex = ofGetWidth() * 0.8;
    int sizey = ofGetHeight() * 0.1;

    ofPushStyle();
    ofPushMatrix();

    ofTranslate(posx, posy, 0);
  
    ofSetColor(225);
    ofDrawBitmapString("Left Channel", 4, 18);
    ofFill();
    ofSetLineWidth(1);	
    ofSetColor(255, 255, 255, 150);
    ofRect(0, 0, sizex, sizey);

    ofNoFill();  
    ofSetColor(245, 58, 135);
    ofSetLineWidth(2);
					
    ofBeginShape();
    for (unsigned int i = 0; i < lAudio.size(); i++){
      float x =  ofMap(i, 0, lAudio.size(), 0, sizex, true);
      ofVertex(x,  sizey * lAudio[i] + (sizey/2));
    }
    ofEndShape(false);
    ofSetColor(58, 135, 245);
    ofBeginShape();
    for (unsigned int i = 0; i < rAudio.size(); i++){
      float x =  ofMap(i, 0, rAudio.size(), 0, sizex, true);
      ofVertex(x, rAudio[i] * sizey + (sizey/2));
    }
    ofEndShape(false);

    ofPopMatrix();
    ofPopStyle();
  }
}

//--------------------------------------------------------------
void ofApp::audioOut(ofSoundBuffer & buffer){
  if (mode_audio){
    
    //float correction = ofMap(ofGetLastFrameTime(), 0, 0.0167, 0, 1);

    float corr = float(n_points) / 1000.0f;
    float v_audio = v * corr;
    float v1_audio = v1 * corr;
    float v2_audio = v2 * corr;
    float v10_audio = v10 * corr;
    float v20_audio = v20 * corr;
    
    for (int i = 0; i < 512; ++i){

	o_s.rotate(v_audio, ofVec3f(0,0,1)); 
	p1_s.rotate(v1_audio, ofVec3f(0,0,1)); 
	p2_s.rotate(v2_audio, ofVec3f(0,0,1)); 
c1_s.rotate(v10_audio, ofVec3f(0,0,1)); 
	c2_s.rotate(v20_audio, ofVec3f(0,0,1)); 
	
	ofPoint pos1_s = c10_s.getGlobalPosition();
	ofPoint pos2_s = c20_s.getGlobalPosition();
	
	int_pos_s = pos1_s + arm_x * (pos2_s - pos1_s).normalize();
	
	ofPoint new_pos = int_pos_s +  arm_y * (pos2_s - pos1_s).getPerpendicular(ofVec3f(0,0,1));

	buffer[i*2] = lAudio[i] =  ofMap(new_pos.x, 0.0, 1920, -0.5, 0.5, true);
	buffer[i*2 +1 ] = rAudio[i] = ofMap(new_pos.y, ll, rr, -0.5, 0.5, true);
	
      }

  }
}
