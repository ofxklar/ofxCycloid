#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxOscParameterSync.h"

class ofApp : public ofBaseApp{

 public:
  void setup();
  void update();
  void draw();

  void presetChanged(int & new_preset);
  void keyPressed(int key);
  void keyReleased(int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void mouseEntered(int x, int y);
  void mouseExited(int x, int y);
  void windowResized(int w, int h);
  void dragEvent(ofDragInfo dragInfo);
  void gotMessage(ofMessage msg);

  void updatePosition();
  void drawMonitor();

  void drawMeca();

  // screen
  
  int width;
  int height;

  int width_max;
  int height_max;

  // fbo

  ofFbo fbo;

  ofTexture fbo_tex;
  ofPixels fbo_pix;
  ofShader shader;

  // GUI
 ofParameterGroup parameters;

  ofxOscParameterSync sync;

  ofxPanel gui;

  ofParameter<int> preset_gui;

  ofParameter<bool> mode_slow;
  ofParameter<bool> draw_nodes;
  ofParameter<bool> draw_monitor;
  ofParameter<bool> draw_gui;


  ofParameter<int> n_points;
  ofParameter<int> n_points_slow;

  ofParameter<int> prec_v;
  ofParameter<int> prec_v1;
  ofParameter<int> prec_v2;

  ofParameter<float> v;
  ofParameter<float> v1;
  ofParameter<float> v2;
  
  ofParameter<float> decrease;
  ofParameter<int> fps;

  ofParameter<float> pos1_x;
  ofParameter<float> pos1_y;
  ofParameter<float> r1;
  ofParameter<float> pos2_x;
  ofParameter<float> pos2_y;
  ofParameter<float> r2;

  ofParameter<float> v10;
  ofParameter<float> v20;
  ofParameter<float> r10;
  ofParameter<float> r20;
  
  ofParameter<float> arm_x;
  ofParameter<float> arm_y;

  ofxPanel gui_presets;
  ofxIntSlider preset;

  // Meca

  float width_meca;
  float height_meca;
  ofPoint pos_meca;
  float meca_ratio;

  ofColor o_color;
  ofColor p1_color;
  ofColor p2_color;
  ofColor arm_color1;
  ofColor arm_color2;
  ofColor b_color;

  int meca_w;

  // Audio

  void setupAudio();
  void audioOut(ofSoundBuffer & input);
  void drawAudio();

  int bufferSize;
  int sampleRate;
  float volume;

  ofSoundStream soundStream;

  vector <float> lAudio;
  vector <float> rAudio;

  ofParameter<bool> mode_audio;
  ofParameter<bool> draw_audio;

  int ll;
  int rr;

  // Cycloid

  float correction;
  ofParameter<bool> draw_cycloid;

  ofPoint center;
  ofPoint last_pos;  

  ofPoint int_pos;

  ofNode o;

  ofNode p1;
  ofNode p2;

  ofNode c1;
  ofNode c2;

  ofNode c10;
  ofNode c20;

  // Cycloid meca

  ofParameter<bool> draw_meca;

  ofPoint center_m;
  ofPoint last_pos_m;  

  ofPoint int_pos_m;

  ofNode o_m;
  ofNode o_m1;
  ofNode o_m2;
  ofNode o_m3;
  ofNode o_m4;

  ofNode p1_m;
  ofNode p2_m;

  ofNode c1_m;
  ofNode c2_m;

  ofNode c10_m;
  ofNode c20_m;
  
  ofNode p1_m2;
  ofNode p2_m2;

  ofNode c1_m2;
  ofNode c2_m2;


  ofPoint pos_m;

  vector<ofPoint> points_m;
  vector<float> rotation_m;
  float rot_cum;
  // Cycloid audio
  int len_m;

  ofNode o_s;

  ofNode p1_s;
  ofNode p2_s;

  ofNode c1_s;
  ofNode c2_s;

  ofNode c10_s;
  ofNode c20_s;

  
  ofPoint int_pos_s;
};
